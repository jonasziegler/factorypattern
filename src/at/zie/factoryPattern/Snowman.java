package at.zie.factoryPattern;

public class Snowman implements Actor{

	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("Snowman is moving");
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		System.out.println("Snowman is rendering");
	}

}
