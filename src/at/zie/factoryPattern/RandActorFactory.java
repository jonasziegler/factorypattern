package at.zie.factoryPattern;

public class RandActorFactory {	
	
	public Actor createActor() {
		int rn;
		Actor newActor = null;
		rn = (int) (Math.random()*((3-1)+1))+1;
		if (rn == 1) {
			newActor = new Snowflake();
		}else if (rn == 2) {
			newActor = new Snowman();
		}else if (rn == 3) {
			newActor = new Star();
		}
		return newActor;
	}
}
