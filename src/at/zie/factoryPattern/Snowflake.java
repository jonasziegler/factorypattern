package at.zie.factoryPattern;

public class Snowflake implements Actor{

	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("Snowflake is falling");
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		System.out.println("Snowflake is rendering");
	}

}
