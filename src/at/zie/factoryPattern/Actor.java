package at.zie.factoryPattern;

public interface Actor {
	public void move();
	public void render();
}
