package at.zie.factoryPattern;

public class Main {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Game g1 = new Game();
		Snowflake sf1 = new Snowflake();
		Snowman sm1 = new Snowman();
		Star str1 = new Star();
		RandActorFactory r1 = new RandActorFactory();
		
		/*
		g1.addActor(str1);
		g1.addActor(sm1);
		g1.addActor(sf1);
		
		g1.renderAll();
		g1.moveAll();
		*/
		r1.createActor();
		g1.addActor(r1.createActor());
		g1.renderAll();
	}

}
