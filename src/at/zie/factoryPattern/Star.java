package at.zie.factoryPattern;

public class Star implements Actor{

	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("Star is moving");
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		System.out.println("Star is rendering");
	}

}
